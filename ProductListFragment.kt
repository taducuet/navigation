package com.example.fresher2022_kotlin_ductm44.androidtraining.assignment7

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fresher2022_kotlin_ductm44.R
import com.example.fresher2022_kotlin_ductm44.androidtraining.assignment5.ProductAsm5
import com.example.fresher2022_kotlin_ductm44.androidtraining.assignment5.ProductRecyclerViewAdapterAsm5
import com.example.fresher2022_kotlin_ductm44.androidtraining.assignment6.ex1.ButtonClick
import java.util.*
class ProductListFragment : Fragment(){
    var listProduct: ArrayList<ProductAsm5> = ArrayList()
    private var _id: String = ""
    private var _name: String = ""
    private var _detail: String = ""
    private var _price: String = ""

    //Sử dụng RecyclerView
    private var productRecyclerViewAdapterAsm5 = ProductRecyclerViewAdapterAsm5(listProduct)
    private var linearLayoutManager = LinearLayoutManager(context)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            _id = it.getString("id").toString()
            _name = it.getString("name").toString()
            _detail = it.getString("detail").toString()
            _price = it.getString("price").toString()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_product_list_nav, container, false)
        val recyclerViewProduct = view.findViewById<RecyclerView>(R.id.recyclerProductNav)

        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        recyclerViewProduct?.adapter = productRecyclerViewAdapterAsm5
        recyclerViewProduct?.layoutManager = linearLayoutManager

        val imagesArray = arrayOf(
            R.drawable.burgerbbq,
            R.drawable.burgerfish,
            R.drawable.burgercheese,
            R.drawable.drinkcoca,
            R.drawable.drinkfanta,
            R.drawable.drinktea,
            R.drawable.pizzachiken,
            R.drawable.pizzahawaii,
            R.drawable.pizzasalami
        )
        val randomImagePosition: Int = Random().nextInt(imagesArray.size)
        val imageId = imagesArray[randomImagePosition]
        listProduct.add(
            ProductAsm5(
                _id.toInt(),
                _name,
                _detail,
                _price.toFloat(),
                imageId
            )
        )
        linearLayoutManager.scrollToPosition(listProduct.lastIndexOf(listProduct.last()))
        for (i in 0 until listProduct.size - 1) {
            if (_id == listProduct[i].ProductId.toString()) {
                listProduct[i].ProductName = _name
                listProduct[i].ProductDetail = _detail
                listProduct[i].ProductPrice = _price.toFloat()
                listProduct.removeAt(listProduct.size - 1)
                linearLayoutManager.scrollToPosition(i)
            }
        }
        productRecyclerViewAdapterAsm5.notifyDataSetChanged()
        return view
    }
}
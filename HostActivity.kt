package com.example.fresher2022_kotlin_ductm44.androidtraining.assignment7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.example.fresher2022_kotlin_ductm44.R

class HostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_host)

        val navHostFragment = NavHostFragment.create(R.navigation.nav_product_management)
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, navHostFragment).setPrimaryNavigationFragment(navHostFragment).commit()
    }
}
package com.example.fresher2022_kotlin_ductm44.androidtraining.assignment7

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.NavHostFragment
import com.example.fresher2022_kotlin_ductm44.R
import com.example.fresher2022_kotlin_ductm44.androidtraining.assignment5.isValidLength
import com.example.fresher2022_kotlin_ductm44.androidtraining.assignment5.validate
import com.example.fresher2022_kotlin_ductm44.androidtraining.assignment6.ex1.ButtonClick

class ProductInputFragment() : Fragment(){

//    private var param1: String? = null
//    private var param2: String? = null
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_product_input_nav, container, false)
        val edtProductId = view.findViewById<EditText>(R.id.edtProductId)
        val edtProductName = view.findViewById<EditText>(R.id.edtProductName)
        val edtProductDetail = view.findViewById<EditText>(R.id.edtProductDetail)
        val edtProductPrice = view.findViewById<EditText>(R.id.edtProductPrice)
        val btnConfirmAsm4 = view.findViewById<Button>(R.id.btnConfirmAsm4)

        btnConfirmAsm4.setOnClickListener {
            if (edtProductId.text.toString().isEmpty()
                || edtProductName.text.toString().isEmpty()
                || edtProductDetail.text.toString().isEmpty()
                || edtProductPrice.text.toString().isEmpty()
            ){
                val builder = context?.let { it1 -> AlertDialog.Builder(it1) }
                builder?.setTitle("Lỗi")
                builder?.setMessage("Vui lòng điền đầy đủ thông tin !")
                builder?.setPositiveButton("OK") { dialog, which ->
                }
                builder?.show()
            }
            else{
                val id = edtProductId.text.toString()
                val name = edtProductName.text.toString()
                val detail = edtProductDetail.text.toString()
                val price = edtProductPrice.text.toString()
                val bundle = Bundle()
                bundle.putString("id", id)
                bundle.putString("name", name)
                bundle.putString("detail", detail)
                bundle.putString("price", price)
                val navController = NavHostFragment.findNavController(this)
                navController.navigate(R.id.productListFragment, bundle)
            }
        }

//        edtProductId.setText(dataId, TextView.BufferType.EDITABLE)
//        edtProductName.setText(dataName, TextView.BufferType.EDITABLE)
//        edtProductDetail.setText(dataDetail, TextView.BufferType.EDITABLE)
//        edtProductPrice.setText(dataPrice, TextView.BufferType.EDITABLE)

        fun valid(input: EditText){
            if (input.text.toString().isEmpty()) input.error = "Đây là thông tin bắt buộc"
            input.validate({ input -> input.isValidLength() }, "Đây là thông tin bắt buộc")
        }

        valid(edtProductId)
        valid(edtProductName)
        valid(edtProductDetail)
        valid(edtProductPrice)
        return view
    }
}